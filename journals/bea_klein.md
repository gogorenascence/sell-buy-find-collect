#Reverse Chronological Order, Latest entries at top

06/09/23
Today was another fantastic day. I got to continue to focus on the design of our website and completed the product detail page. Yes, that took me all day. It was very worth it though! Even though the page itself is still quite basic, it is the most amount of react component, html, css, and bootstrap that I have done before. We merged and submitted our beautifully deployed project. Really proud of everyone in my group, I got placed with some amazing people, and I'm grateful we got to experience this together through good and bad. 

06/08/23
Today was such a successful day. We finally figured out the issues we were having with the cart, implemented so many features we had been struggling with, and I got to move on to frontend design! My favorite. I've really been wanting to strengthen my frontend design skills, so getting to work on this has been fantastic. I fixed our nav and got our homepage 80% there. I learned how to convert a table into a grid, including the mapping of our data from one to the other. Super proud of that. Tomorrow we'll likely be working stretch goals!

06/07/23
Today we cleaned up and started the process of deploying our app. Fixing all the commented out code and unused variables took a decent amount of time, but we were done early afternoon. Finishing deployment was such an accomplishment! It took alot of consultng with other groups and troubleshooting, but we successfully deployed. Felt amazing to be done with such a difficult maze of a task.

06/06/23
Today we focused on unit tests and beginning deployment. We wrote all 4 of our unit tests, one each, and helped each other along the way. It was very satisfying to watch our tests pass. The rest of the day was then spent going over deployment and how to begin that process. Tomorrow we should be able to troubleshoot the actual deployment of the app.

06/05/23
Today we still could not figure out our shopping cart and quantity issues. We decided for the sake of getting things done, to move on to protecting our routes throughout our application. We successfully implemented authorization for many links and a few buttons as well. Tomorrow we will continue with our shopping cart.

06/02/23
Today we wrestled with the shopping cart. We did get several buttons and their functionality working, but still need to get our quantity functionality working in order to complete the cart. We are so close! Hoping to be done with it on Monday.

06/01/23
Today we updated store and product forms and got some help on passing parameters through api query.

05/31/23
Today I navigated for the first half of the day while we began working in our edit store form. We were fairly stuck most of the day researching documentation on how to write the update code in the frontend. Had a good step forward in creating extra models for StoreUpdate and StoreOutUpdate which gave us a 200 response, which we were not expecting! Hoping to get some help tomorrow on update frontend so we can move on.

05/30/23
Today I caught up on everything that was implemented on Friday, and took an observer seat while we continued to go through the code so that I would fully understand what we had written. I am getting a better grasp on Redux, creating Queries, referencing repos, and accessing data in our Frontend. We continued with several views and at the end of this day, we have our MVP completed except for our shopping cart. We hope to do the cart tomorrow.

05/26/23
Today I was not in class, but Steve, Conor, and Frances continued with frontend authentication, as well as creating several forms and list views for the project

05/25/23
Today we moved onto frontend authentication. The day began with referencing other examples of frontend using Mongo and Redux, and we quickly realized just how much e didn't know. We took time to study up on Redux, and started slowly implementing the code. We were able to set up one form and begin our Router tree.

05/23/23
Today I drove the first half of the day while we tackled our shopping cart functionality in the backend. At first we really struggled on how to get everything together, but we learned from our past mistakes in relationship building in MongoDB, worked backwards, and configured our shopping cart intelligently. Frances drove in the second half of the day while we finalized a few backend checkout functions as well as create a Purchase model. Tomorrow we plan on moving to our frontend authentication.

05/22/23
Today Conor and Steve took turns navigating while the rest of the team drove. We connected our models together, account to store, and store to products. We also set up our entire CRUD for our accounts to make the user experience much more pleasant with more functionality. We are almost done with our backend! Tomorrow we will do our shopping cart CRUD, then we can move on to front-end.

05/18/23
Today Conor was navigating while the rest of the team drove. We set up our next model for Products and got all of that running on the backend. Our big hurdle was to figure out the relationship building between models in MongoDB, and after many long sessions with SEIRS, we were able to figure out how to connect them. I had the logic right in my head, and how to for loop through it all, but it was the nuances of converting the ids between bson and json that I didn't have a proper grasp on. But now we have a good template for how to do it, figured out how to connect our stores to our products, and then Monday we'll be connecting our users to their stores.

05/17/23
Today we had me sharing my screen and navigating for the first half, where we set up all of our Authorization for our app. We worked very quickly and accomplished our log-in, log-out, and sign-up functionalities. The second half of the day Steve was navigating with the rest of us driving. We completed work on the CRUD for our Store model. We celebrated each time we got an endpoint to work. Really proud of the team's work today, as we finished much more than we initially set out to.

05/16/23
Today the four of us all worked on getting our models set up on our back-end. We talked a-lot about Foreign Key relationships using a MongoDB and how to go about writing ours. We were pleasantly surprised after researching and referencing other code that it is much easier than we initially thought! Simply adding the id parameter and then connecting it within queries.

05/15/23

Today we have Steve sharing his screen and typing, while
we all research MongoDB and get our database set up. We completed
the database set up and are planning on beginning authorization tomorrow.
