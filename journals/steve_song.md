## June 9, 2023

Today, I worked on:

- I was the driver in the morning, conor around lunchtime, Frances after lunch, and Bea was on her own adventure making our application BEA-UTIFUL! We finished adding purchase history and details, format code, update api endpoint naming convention, remove comments, console logs and print statements, update models, update endpoints to store total cost, fixed ids, deleted unnecessary files, and finalized deployment.

Our group discussed last minute functionalities we could implement today before the deadline, and we succeeded in adding everything but the sales history for store owners.

Today, I became more familiar with creating backend endpoints, cleaning up code, and working with logic on the front and backend. 

## June 8, 2023

Today, I worked on:

- I was the driver today with the rest of the group as support. We were able to fix the issue where the product wasn't being added to the cart on first cart creation. We then proceeded to add functionality for displaying the total price on the cart page, checking if a product already exists in the cart which then adjusts the quantity instead of adding a duplicate of the product. We added edit buttons for editing the store and adjusted the tag invalidation to refetch data on update. Bea was the driver for adding styling to our store page.

Our group discussed how we could implement quantity and price, and decided to implement it on the backend, for easier manipulation on the front end.

Today, I became more familiar with implementing logic on the backend.

## June 7, 2023

Today, I worked on:

- I was the driver today with the rest of the group as support. We were able to finish deploying our application on Cirrus. Bea was the driver at the end of the day, and we worked won the README after finishing deployment.

Our group discussed how we could best write the readme for our application. We decided to reference the example in Learn to help us with this.

Today, I became more familiar with deployment and gitlab pipelines/jobs.

## June 6, 2023

Today, I worked on:

- I was the driver today with the rest of the group as support. We were able to finish creating the unit tests for our application and begin working on deploying our application.

Our group discussed the best plan of action to finish requirements before the deadline on Friday.

Today, I became more familiar with writing unit tests.

## June 5, 2023

Today, I worked on:

- I was the driver today with the rest of the group as support. We continued to struggle with implementing the add to cart feature. We moved on to the nav bar after we weren't able to make any progress.

Our group discussed about how we could implement adding quantity when adding the product to the cart. One idea we thought about was creating an endpoint that takes in the ProductIn and a second parameter for quantity to create a new item that has both.

Today, I became more familiar with hiding links in the navbar when certain conditions are met.

## June 2, 2023

Today, I worked on:

- Conor was the driver for today with the rest of the group as support. We tried implementing the add to cart feature, however we faced an issue with writing the conditional that checks if the user has a cart created already, and if not, create the cart, then immediately after add product to the cart.

Our group discussed how we could implement the add to cart feature on the front end. We faced a blocker early on with using useEffect.

Today, I became more familiar with experimenting in react.

## June 1, 2023

Today, I worked on:

- Frances was the driver in the morning, Conor was the driver mid day, and I was the driver at the end of the day. We worked on finishing the edit store form. We created the edit user profile and view cart products page.

Candice can in today and taught how to use lazyquery to help us pass in the store_id into the update mutation and get rid of the CORS error.

Today, I became more familiar with using the lazyquery function in react.

## May 31, 2023

Today, I worked on:

- Bea was the driver in the morning on creating the edit store form page, and I was the driver for the same feature in the afternoon. However, we currently have a blocker where we are not able to pass in the store_id into the query parameters, resulting in a CORS error because the store_id in the url is undefined.

We tried discussing ways we could pass in the store_id, but were unsuccessful with coming up with a way to pass in the store_id. We are hoping that Candice will be able to help us figure this out.

Today, I became more familiar with the query function in the api.js file.

## May 30, 2023

Today, I worked on:

- Conor was the driver in the morning and I was the driver in the morning. We finished implementing the detail pages for store and products.

We tried working with the SEIRs to figure out a CORS error we were getting because a variable we have was not loading immediately. However after a few milliseconds, it loads and so the page actually renders correctly.

Today, I became more familiar with the useParams function.

## May 26, 2023

Today, I worked on:

- Frances was the driver for working on the frontend. We were able to create the create_product page and write a function to list the products by getting the store_id in accounts. We faced a blocker early on that prevented us from moving forward. We don't know how to pass in a id to functions that require an id in their parameter.

We tried to read up on documentation to see how we could pass in the store_id to our list_products page, but were unsuccessful in doing so.

Today, I became more familiar with creating functions in FastApi.

## May 25, 2023

Today, I worked on:

- I was the driver for completing the frontend authentication and the rest of the group was support. We were able to finish the login, signup, logout, and create store pages.

We discussed about how we can implement the authentication, and ways we can use the query to create and access the token. We ended up referencing the mongo-api-example to help us along with Candice's help.

Today, I became more familiar with we can create and access the token once logged in.

## May 24, 2023

Today, I worked on:

- I was the driver for starting the frontend authentication, but we made zero progress on this front. The rest of the group was the support in helping navigate that. As a result of getting stuck, we decided to spend the rest of the afternoon before the final attendance on reading up on redux through documentation and videos. After the final attendance, I went through the redux walkthrough on Learn with Frances and Conor and built a functional store list and store form using redux. We were unable to submit the form because we didn't implement frontend authentication.

We discussed about whether we should use redux or the jwt-down library provided by Hack Reactor. Many agreed that using redux in this project would be to our benefit since Bea shared that many software developer friends of hers used redux on the job. After trying to blindly figure out how implement frontend authentication using redux with Candice's example, we decided to stop and then learn how redux works first before moving forward.

Today, I became more familiar with how redux works. The relationship between store, action, dispatch, reducers, and middleware has become more clear.

## May 23, 2023

Today, I worked on:

- Bea was the driver for starting the CRUD functionality for our cart model. The rest of the group was the support in helping navigate that. On the second half of the day, Frances was the driver for finishing the CRUD functionality for our cart model and adding functions to assign/remove a cart to/from an account.

We discussed about how we should setup the relationships between the cart and our stores/accounts/products, and decided to add the cart id to the account as a property and then on checkout append the cart id to the purchases property on the model. We create a cart and then append products to the cart. On the frontend, we will be applying logic to make sure if the cart doesn't exist in the account, then a cart will be created and then assigned to the cart id field.

Today, I became more familiar with setting up relationships within MongoDB.

## May 22, 2023

Today, I worked on:

- Conor was the driver for finishing the CRUD functionality for our products model. The rest of the group was the support in helping navigate that. On the second half of the day, I was the driver for finishing the CRUD functionality for our accounts model and adding functions to assign/remove a store to/from an account.

We asked for clarification on how we should set up our relationships between our models, and one of our Instructors(Andrew) helped us to simplify our relationships starting from a root model and then to go from there. For example: Accounts -> Stores -> Products. We decided to create a separate function to add the product to the store's product list AFTER the product has been created.

Today, I learned that if we are referencing documents in another collection, we should just use the id, and then reference that id in the specific collection.

## May 18, 2023

Today, I worked on:

- Conor was the driver for creating CRUD functionality for our products model. The rest of the group was the support in helping navigate that.

As Conor was implementing CRUD functionality for our products model, we got a little stuck on connecting our products to our stores.

Today, I learned how to reference different collections in our database.

## May 17, 2023

Today, I worked on:

- Bea was the driver for setting up sign-up, log in, and log out authentication. The rest of the group was the support in helping navigate that. I was the driver for creating the CRUD functions for our store model and the rest of the group supported in creating those functionalities.

As I was implementing CRUD functionality for our store model, the group realized that we were referencing the query functions in our routers file. Also, we discussed about how our functions and calls should be written using the resources online and in learn.

Today, I learned about how quickly we can check our endpoints because of FastAPI. No need for insomnia!

## May 16, 2023

Today, I worked on:

- Adding the model properties for accounts, products and store with Frances as the driver and the rest of the group as support.

As we were filling out the model properties, we found that we will need to connect the models to each through their id.

Today, I learned that we can add an excalidraw file to VSCode and see the wireframe we created by installing the excalidraw extension.

## May 15, 2023

Today, I worked on:

- Implemented the starting steps for MongoDB with myself as the driver and the rest of the group as support.

The group had a discussion on the MongoDB and PostgreSQL to determine which one we would use for the project. We needed more clarity on the differences between the two from the SEIRs and decided on using MongoDB.

Today, I used the shortcut to highlight a section of code and move it with the arrow keys.
