(I didn't realize we were supposed to be pushing our journals daily and was keeping my journals locally in a note file, so I apologize for the sporadic commits.)

## June 9, 2023
Another great day! We got so much done again. We are all very thankful that we got deployment out of the way so early and had the opportunity to continue to working on our app's functionality and features. Steve, Frances, and I all drove for a bit. Bea was making our site beautiful working on the CSS all day. We fixed the formatting for our entire project, cleaned up comments, removed console logs and prints, implemented purchase history and purchase detail pages, inserted a cart icon into our nav bar using react-icons (that was cool!), updated our API endpoint naming conventions, and figured out how to store total cost in our cart objects. There's even more I'm probably forgetting. We would like to implement third-party API integration next week, as that was the one big feature I felt we never crossed off our list. This whole week has been exhausting but so productive, and I couldn't be prouder of our team and our final submission.

## June 8, 2023
Today was a great day! We've had good days and bad ones during this module, but today was fun and productive from start to finish. We really got so much done that had been holding us up. Candice helped us finally solve the "add to cart" issues. Everything works now! That was really exciting. We spent the rest of the day tackling other cart-related issues and were just totally in the zone. Everything seemed easy today. We fixed bug after bug all day and even got our "check out" button to work! It was nice to take the last two days to focus on unit tests and deployment so that we could take a deep breath before coming back to the shopping cart. Bea worked on adding styling and design to our site today, too, so everything is really coming together and starting to look nice. We just have some finishing touches to add tomorrow.

## June 7, 2023
I finally got protected routes to fully work after a tip from Cape and a little more research! I was doing it wrong but close, so it was really satisfying getting that done. Steve drove again today, and we completed deployment! Steve is the MVP right now. He says he hates it, but I think he's got a knack for this devops stuff. Again, we followed along with Andrew and Learn and deployment wasn't *too* painful. We spent some time with other groups and helping each other along. Andrew Ward helped us a lot with deployment. It seems like everyone is dealing with the same issues.  Our site basically has no CSS right now, but everything kinda works and is deployed... It was awesome to pull up the url for our deployed app on my phone and see our project on a different screen and without having to run Docker. I'm pretty fuzzy on a lot of the steps in deployment still, but finally getting all those jobs to run was so satisfying. We started to write the README at the end of the day, with Bea leading the way. Only two days left! All there's left to do is make things look pretty and try to tame the shopping cart finally!

## June 6, 2023
We all completed our unit tests! We just watched along with Andrew's lectures, really. We hit some snags along the way, but nothing too crazy. Next, we started the deployment process. Steve drove. Pretty "boring" day as far as development goes, but we want to get all of the important stuff out of the way so that we are not scrambling later this week.

## June 5, 2023
The cart struggles continue. I have so much newfound respect for ecommerce engineers. How do they do this stuff? We have tried so many fixes for this "add to cart" issue and are pretty clueless right now. We have started to try to figure out the "quanity" for a product to be added to the cart. We have a "counter" that shows, but we are unsure how to use it or implement quantity on the back end. In order to alleviate the mental anguish of the cart, we fixed up the rest of our nav bar so that users/non-users see the appropriate links. I worked for a little bit at night to set up protected routes for our site. Right now, we don't have any security in place, and non-logged in users can access pages they shouldn't be able to (such as create store). I've sorta made this my pet project and have strangely enjoyed trying to solve this puzzle. I find it weird that React Router doesn't have a built-in method for this!

## June 4, 2023
Steve, Frances, and I had a quick planning session Sunday night, which was pretty cool! It was nice to take some time to plan out the week and figure out what we still need to tackle.

## June 2, 2023
Today was ROUGH. I drove and we spent most of the the day struggling with the shopping cart. The issue is we want the "add to cart" button to create a cart for user if one does not already exist (in addition to adding the product to the cart), but the operations run so "simultaneously" that the newly created card_id is not yet available when the "add product to cart" function runs. You have to press the button twice right now: first to create the cart and second to add the product to the cart. And that is no bueno. Perhaps we don't understand useEffect at all? This issue has been our biggest blocker yet.

## June 1, 2023
Candice to the rescue... again. Deja vu all over again. She quickly helped us solve our issue of passing ids in our update mutations. She also walked us through the CORS console error we were receiving. We were right about what was causing it, so that was reassuring. Candice introduced us to LazyQuery. Great name aside, it fixed the error. In the morning, Frances drove, then I drove. In the afternoon, Steve drove. We had some late day breakthroughs! We got created/edit user profile to work AND got the "add to cart" button to work. We were also able to display all of the products in the cart, and Steve and I were able to add "remove item" and "clear cart" buttons, which was pretty sweet! I worked at night on some things to make our lives easier in the future: nested the App.js routes, fixed the form redirects, and cleaned up our previously insane Nav bar.


## May 31, 2023
Man... today was a tough one. The state is throwing us off. Bea drove in the morning, and Steve drove in the afternoon. We tried so much stuff to get our edit store form to work. We cannot figure out how to properly pass ids... again. We *kinda* know what's going on but are unsure how to fix it. At least we know where to pick up tomorrow.

## May 30, 2023
Candice to the resuce! I drove, and Candice came in and sorted out our issues for us so quickly. We were able to easily reference the current page's id using the useParams hook. Total breakthrough for us. We had to change a bunch of code around on our back end based on some recommendations Candice made, and everything makes so much more sense now. We got SO much done after that. We dealt with an annoying CORS error later in the day, but it's not affecting any functionality. On to the beast that is the shopping cart tomorrow.

## May 26, 2023
Today, we continued on with the front end. Frances was driver. We started with the product creation form and then tried to display all of the products for a specific store. This first required a back end function to return the list of the product details and not just ids. We struggled for a while trying to reference the store id. IDs have been the bane of our existence lately. We need Candice's help on Monday figuring out Redux. It's been tough learning it so far. All in all, a good day despite the blockers we encountered.

## May 25, 2023
Today, we tackled front end authentication again. We continued to study up on Redux and reference other examples of Mongo/Redux in the morning. Steve drove early on, and we were able to finally create a basic login form and a logout button. I drove in the afternoon and at night. We had a lot of success but stumbled into some issues with id relationships with our posts on the front ends. Korenbari and Hanna tried to help us for a bit.

## May 24, 2023
Today, we tried to start front end authentication. That ended quickly! We decided to all take a break and read up on Redux since it's all so new to us. Steve and I ended up staying on Zoom until 9pm messing around with Redux and it started to click a little bit.

## May 23, 2023
Today, we finally tackled our shopping cart model and CRUD. Bea drove in the morning, Frances the afternoon. We knew this one was going to be a doozy, and... it is. The relationships seem complex right now, but we think we are headed in the right direction. We are just treating the cart similarly to a store. We think it may have been easier to use a SQL database, but on we go... We were able to succesffuly create the cart CRUD and assign carts to users and products to carts. We ended the day trying to plan out how to "check out" a cart and how our planned user purchase history and user sales history data will eventually be implemented on the front end. That's basically it for the back end, and we plan on starting front end authentication tomorrrow.

## May 22, 2023
Today, we worked on our update functionality and finally sorted out how our model relationships are going to work. I drove again today, and it was a pretty frustrating afternoon until we had some late-day breakthroughs. Andrew helped set us straight in terms of our model relationships and how to best implement our update functionality. What would we be doing without Kornebari so far? I truly don't know... Later in the day, we finished some of the queries/routers for the accounts model that we hadn't finished when we first set up accounts, including edit profile, delete account, etc. Overall, it was a long day, but it ended on a high note and we are ready to go for tomorrow.

## May 18, 2023
Today, we worked on our product CRUD. I was driver finally after feeling sick earlier in the week. It wasn't that bad! We got everything to work, but we are getting lost in the relationships between the data. Kornebari was so helpful again; he's basically going to be an honorary 5th member of our team by the end of the project. We have a lot to figure out next week, but it is all coming to us slowly but surely. Mongo is starting to click, even though I often find myself getting stuck thinking relationally.

## May 17, 2023
Today, we worked on implementing authentication via JWTdown and our store CRUD. Bea and Steve split up driving duties today, and we successfully added sign up, log in, log out, and authentication tokens. We also completed the CRUD for our store model: create store, get store, get all stores, update store, and delete store. We are really starting to get a hang of the workflow using FastAPI, MongoDB, and PyMongo, and today was a great day for morale.

## May 16, 2023
Today, we created our models for accounts, stores, and products. Bea drove today, and we are starting to see the differences between relational and non-relational databases. We are going to need to link our models via an id, and... we don't know how to do that yet! But it's okay because it seems like it will be fairly straightforward to implement.

The lack of guidance has been frustrating thus far, but I think our group has been doing great despite that. Kornebari has been SO helpful.

## May 15, 2023
Today, we discussed our choice of database and settled on MongoDB. Steve and Frances drove today, and we got everything set up to use Mongo (installed Compass, updated the yaml and requirements, etc.)

The SEIRs were very helpful in clarifying the differences between PostgreSQL and MongoDB and helping us settle on Mongo.
