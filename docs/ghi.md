Customer Graphical Human Interface

Home Page
__________
This will be the first page visitors arrive to on the website, which is a list of all the stores on the website. The user will have the option to sign up or log in to an already existing account. The user can browse all the stores and go to a specific store detail page from the home page.

![Home Page](wireframes/homepage.PNG)

Store Page
__________
The store detail page lists all the products from a specific store. The user can browse the products and get to the product detail page from here.

![Store Page](wireframes/storepage.PNG)

Product Page
__________
The product page lists all the details of the specific product, allows you to choose your quantity of the item you would like, and add the product to the cart (click twice)

![Product Page](wireframes/productdetailpage.PNG)

Accounts Pages
__________
A user can view their profile, navigate to their cart, edit their profile, and create a store.

![Profile Page](wireframes/profilepage.PNG)

Cart/Checkout
__________
Can only checkout as a logged-in user.

![Cart Page](wireframes/cartpage.PNG)
