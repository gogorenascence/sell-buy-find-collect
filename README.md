# SBFC - Sell Buy Find Collect

* Steve Song
* Conor Leister
* Bea Klein
* Frances Wong

SBFC – Sell your items, Buy more items, Find even more items, Collect all the items!


## Design

- [API Design/Data Model](/docs/apis.md)
- [GHI](/docs/ghi.md)
- Integrations (in progress during stretch goal week)


## Intended market
We are targeting general consumers who are interested in finding unique items unlike any they've seen before as they come from small businesses. Our website is also intended for those small businesses to capture their own niche market within our user base.

## Functionality

Visitors to the site can browse stores and products available, as well as create their own profile in order to buy items. Users can also create their own stores to sell their wares.


Users can click on stores to view that specific stores products, and then click on the product to go to product detail page and add their product to the cart.

Stores list as a plain list view
Products as a plain list view

Accounts
Users can create their own profile, edit the profile, create their store, add products to their store, and checkout from other stores.

The cart features products that were added from the product page


## Project Initialization
To fully enjoy this application on your local machine, please make sure to follow these steps:

Clone the repository down to your local machine
CD into the new project directory
Run docker volume create mongodata

Run docker compose build

Run docker compose up

Run docker exec -it module-3-project-gamma-fastapi-1

Exit the container's CLI, and enjoy SBFC to its fullest!
