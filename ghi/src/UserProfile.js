import { useParams, Link } from "react-router-dom";
import { useGetTokenQuery, useGetUserQuery } from "./store/api";
import { useEffect, useState } from "react";


function UserProfile() {
  const { account_id } = useParams();
  const { data } = useGetUserQuery(account_id);
  const { data: tokenData } = useGetTokenQuery();
  // eslint-disable-next-line
  const [zodiacData, setZodiacData] = useState(null);

  const sign = tokenData?.account?.zodiac;
  console.log("sign: ", sign);

  const fetchData = async () => {
    const url = `https://cors-anywhere.herokuapp.com/https://ohmanda.com/api/horoscope/${tokenData?.account?.zodiac}/`;
    const headers = {
      Origin: "http://localhost:3000",
    };
    const response = await fetch(url, { headers });
    if (response.ok) {
      const zodiac_data = await response.json();
      setZodiacData(zodiac_data);
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <section className="h-100 h-custom">
        <div className="container h-100 py-5">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col">
              <div className="text-center mb-5">
                <h1 style={{ "font-weight": "380" }}>My Account</h1>
              </div>
            </div>
            <div className="d-flex">
              <div className="card border-light mb-3 col-4">
                <div className="card-body">
                  <h5
                    className="card-title text-center mt-3 mb-4"
                    style={{ "font-weight": "380" }}
                  >
                    ACCOUNT DETAILS
                  </h5>
                  <div className="text-center">Username: {data?.username}</div>
                  <div className="text-center mt-3">Email: {data?.email}</div>
                  <div className="text-center mt-3">Zodiac: {data?.zodiac}</div>
                  <div className="text-center mt-4 mb-3">
                    {tokenData?.account?.id === account_id ? (
                      <Link
                        className="btn btn-outline-dark mt-auto btn-sm"
                        to={`/accounts/${account_id}/edit`}
                      >
                        Edit My Profile
                      </Link>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
              <div className="card border-light mb-3 col-4">
                <div className="card-body">
                  <h5
                    className="card-title text-center mt-3 mb-4"
                    style={{ "font-weight": "380" }}
                  >
                    MY STORE
                  </h5>
                  <div className="text-center">
                    {tokenData?.account?.store_id != null ? (
                      <Link
                        className="btn btn-outline-dark mt-auto btn-sm"
                        to={`../../stores/${data?.store_id}`}
                      >
                        View Store
                      </Link>
                    ) : (
                      <Link
                        className="btn btn-outline-dark mt-auto btn-sm"
                        to={`../../stores/new`}
                      >
                        Create Store
                      </Link>
                    )}
                  </div>
                  <div className="text-center mt-3">
                    {tokenData?.account?.store_id != null ? (
                      <Link
                        className="btn btn-outline-dark mt-auto btn-sm"
                        to={`../../stores/${data?.store_id}/edit`}
                      >
                        Edit Store
                      </Link>
                    ) : (
                      ""
                    )}
                  </div>
                  <div className="text-center mt-3">
                    {tokenData?.account?.store_id != null ? (
                      <Link
                        className="btn btn-outline-dark mt-auto btn-sm"
                        to={`../../products/new`}
                      >
                        Add Product
                      </Link>
                    ) : (
                      ""
                    )}
                  </div>
                </div>
              </div>
              <div className="card border-light mb-3 col-4">
                <div className="card-body">
                  <h5
                    className="card-title text-center mt-3 mb-4"
                    style={{ "font-weight": "380" }}
                  >
                    ORDER HISTORY
                  </h5>
                  <div className="text-center">
                    {tokenData?.account?.purchases.length === 0 ? (
                      "You haven't placed any orders yet."
                    ) : (
                      <Link
                        className="btn btn-outline-dark mt-auto btn-sm"
                        to={`purchases/`}
                      >
                        View Orders
                      </Link>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default UserProfile;
