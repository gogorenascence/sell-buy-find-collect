import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useEditStoreMutation } from "./store/api";
import { useNavigate } from "react-router-dom";


function EditStoreForm() {
  const { store_id } = useParams();
  const [store_name, setStoreName] = useState("");
  const [description, setDescription] = useState("");
  const [img_url, setImgUrl] = useState("");

  const fetchData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/api/stores/${store_id}`;
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setStoreName(data.store_name);
      setDescription(data.description);
      setImgUrl(data.img_url);
    }
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (result.isSuccess) {
      navigate(`/stores/${store_id}`);
    }
  });

  useEffect(() => {
    fetchData();
  // eslint-disable-next-line
  }, []);

  const [editStore, result] = useEditStoreMutation();

  const handleStoreNameChange = (event) => {
    const value = event.target.value;
    setStoreName(value);
  };

  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  };

  const handleImgUrlChange = (event) => {
    const value = event.target.value;
    setImgUrl(value);
  };


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      data: {
      store_name,
      description,
      img_url,
    },
      store_id,
    }
    editStore(data);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Edit Your Store</h1>
          <form onSubmit={handleSubmit} id="create-store-form">
            <div className="form-floating mb-3">
              <input
                value={store_name}
                onChange={handleStoreNameChange}
                placeholder="Store Name"
                required
                type="text"
                id="store_name"
                className="form-control"
              />
              <label htmlFor="store_name">Store Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={description}
                onChange={handleDescriptionChange}
                placeholder="Description"
                required
                type="text"
                id="description"
                className="form-control"
              />
              <label htmlFor="description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={img_url}
                onChange={handleImgUrlChange}
                placeholder="Image Url"
                type="url"
                id="img_url"
                className="form-control"
              />
              <label htmlFor="img_url">Image Url</label>
            </div>
            <button className="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditStoreForm;
