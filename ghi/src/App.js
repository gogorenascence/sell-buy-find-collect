import React from "react";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Nav from './Nav';
import "./App.css";
import { useGetTokenQuery } from "./store/api";
import ListStores from "./ListStores";
import CreateStoreForm from "./CreateStoreForm";
import LoginForm from "./LoginForm";
import SignUpForm from "./SignUpForm";
import CreateProductForm from "./CreateProductForm";
import ListStoreProducts from "./ListStoreProducts";
import ProductDetail from "./ProductDetail";
import EditStoreForm from "./EditStoreForm";
import EditProductForm from "./EditProductForm";
import UserProfile from "./UserProfile";
import EditUserProfile from "./EditUserProfile";
import ShoppingCart from "./ShoppingCart";
import PurchaseHistory from "./PurchaseHistory";
import PurchaseDetail from "./PurchaseDetail";


function App() {
  const { data: tokenData } = useGetTokenQuery();
  const domain = /https:\/\/[^/]+/;
  const basename = process.env.PUBLIC_URL.replace(domain, "");

  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="*" element={<Navigate to="/" replace={true} />} />
          <Route path="login" element={<LoginForm />} />
          <Route path="signup" element={<SignUpForm />} />
          <Route path="/" element={<ListStores />} />
          <Route path="stores">
            <Route path=":store_id" element={<ListStoreProducts />} />
            <Route
              path="new"
              element={
                tokenData ? (
                  <CreateStoreForm />
                ) : (
                  <Navigate to="/login" replace={true} />
                )
              }
            />
            <Route
              path=":store_id/edit"
              element={
                tokenData ? (
                  <EditStoreForm />
                ) : (
                  <Navigate to="/login" replace={true} />
                )
              }
            />
          </Route>
          <Route path="products">
            <Route path=":product_id" element={<ProductDetail />} />
            <Route
              path="new"
              element={
                tokenData ? (
                  <CreateProductForm />
                ) : (
                  <Navigate to="/login" replace={true} />
                )
              }
            />
            <Route
              path=":product_id/edit"
              element={
                tokenData ? (
                  <EditProductForm />
                ) : (
                  <Navigate to="/login" replace={true} />
                )
              }
            />
          </Route>
          <Route path="accounts">
            <Route
              path=":account_id"
              element={
                tokenData ? (
                  <UserProfile />
                ) : (
                  <Navigate to="/login" replace={true} />
                )
              }
            />
            <Route
              path=":account_id/edit"
              element={
                tokenData ? (
                  <EditUserProfile />
                ) : (
                  <Navigate to="/login" replace={true} />
                )
              }
            />
            <Route
              path=":account_id/purchases"
              element={
                tokenData ? (
                <PurchaseHistory />
                ) : (
                <Navigate to="/login" replace={true}
                />
                )
              }
            />
            <Route
              path=":account_id/purchases/:cart_id"
              element={
                tokenData ? (
                <PurchaseDetail />
                ) : (
                <Navigate to="/login" replace={true}
                />
                )
              }
            />
          </Route>
          <Route
            path="cart/:cart_id/"
            element={
              tokenData ? (
                <ShoppingCart />
              ) : (
                <Navigate to="/login" replace={true} />
              )
            }
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
