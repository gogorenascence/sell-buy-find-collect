import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    username: '',
    password: '',
};

export const accountSlice = createSlice({
    name: 'account',
    initialState,
    reducers: {
        clearForm: () => {
            return initialState;
        }
    },
});

export const {
    clearForm,
} = accountSlice.actions;
