import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    store_name: '',
    description: '',
    img_url: ''
};

export const storeSlice = createSlice({
    name: 'store',
    initialState,
    reducers: {
        storeUpdated(state, action) {
            const { id, store_name, description, img_url } = action.payload
            const existingStore = state.find(store => store.id === id)
            if (existingStore) {
                existingStore.store_name = store_name
                existingStore.description = description
                existingStore.img_url = img_url
            }
        }
    },
});

export const {
    storeUpdated,
} = storeSlice.actions;

export default storeSlice.reducer;
