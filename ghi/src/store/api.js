import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { clearForm } from './accountSlice';

export const apiSlice = createApi({
  reducerPath: "stores",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
    prepareHeaders: (headers, { getState }) => {
      const selector = apiSlice.endpoints.getToken.select();
      const { data: tokenData } = selector(getState());
      if (tokenData && tokenData.access_token) {
        headers.set("Authorization", `Bearer ${tokenData.access_token}`);
      }
      return headers;
    },
  }),
  tagTypes: ["Store", "Token", "Account", "Product", "Cart"],
  endpoints: (builder) => ({
    getStores: builder.query({
      query: () => "/api/stores/",
      providesTags: ["Store"],
    }),
    getStore: builder.query({
      query: (store_id) => `/api/stores/${store_id}`,
      providesTags: ["Store"],
    }),
    createStore: builder.mutation({
      query: (data) => ({
        url: "/api/stores/",
        body: data,
        method: "post",
      }),
      invalidatesTags: ["Store", "Token"],
    }),
    editStore: builder.mutation({
      query: (data) => ({
        url: `/api/stores/${data.store_id}`,
        body: data.data,
        method: "put",
      }),
      providesTags: ["Store"],
      invalidatesTags: ["Store"],
    }),
    createProduct: builder.mutation({
      query: (data) => ({
        url: "/api/products/",
        body: data,
        method: "post",
      }),
      invalidatesTags: ["Product"],
    }),
    editProduct: builder.mutation({
      query: (data) => ({
        url: `/api/products/${data.product_id}`,
        body: data.data,
        method: "put",
      }),
      providesTags: ["Product"],
      invalidatesTags: ["Store", "Product"],
    }),
    removeProductFromStore: builder.mutation({
      query: (data) => ({
        url: `/api/stores/${data.store_id}/remove_product/${data.product_id}`,
        method: "put",
      }),
      invalidatesTags: ["Store"],
    }),
    getStoreProducts: builder.query({
      query: (store_id) => `/api/stores/${store_id}/products/`,
      providesTags: ["Store"],
    }),
    getProduct: builder.query({
      query: (product_id) => `/api/products/${product_id}`,
      providesTags: ["Product"],
    }),
    getCart: builder.query({
      query: (cart_id) => `/api/cart/${cart_id}`,
      providesTags: ["Cart"],
    }),
    getCartProducts: builder.query({
      query: (cart_id) => `/api/cart/${cart_id}/products/`,
      providesTags: ["Cart"],
    }),
    editCart: builder.mutation({
      query: (data) => ({
        url: `/api/cart/${data.cart_id}`,
        body: data.data,
        method: "put",
      }),
      providesTags: ["Cart"],
      invalidatesTags: ["Cart"],
    }),
    createCart: builder.mutation({
      query: (data) => ({
        url: "/api/cart/",
        body: data,
        method: "post",
      }),
      invalidatesTags: ["Cart", "Token"],
    }),
    addProductToCart: builder.mutation({
      query: (data) => ({
        url: `/api/cart/${data.cart_id}/add_product/${data.product_id}/${data.quantity}`,
        method: "put",
      }),
      invalidatesTags: ["Cart"],
    }),
    removeProductFromCart: builder.mutation({
      query: (data) => ({
        url: `/api/cart/${data.cart_id}/remove_product/${data.product_id}`,
        method: "put",
      }),
      invalidatesTags: ["Cart"],
    }),
    clearCart: builder.mutation({
      query: (cart_id) => ({
        url: `/api/cart/${cart_id}/clear_cart`,
        method: "put",
      }),
      invalidatesTags: ["Cart"],
    }),
    checkoutCart: builder.mutation({
      query: (account_id) => ({
        url: `/api/accounts/${account_id}/checkout_cart`,
        method: "put",
      }),
      invalidatesTags: ["Account", "Cart", "Token"],
    }),
    getUser: builder.query({
      query: (account_id) => `/api/accounts/${account_id}`,
      providesTags: ["Account"],
    }),
    getUserPurchases: builder.query({
      query: (account_id) => `/api/accounts/${account_id}/purchases`,
      providesTags: ["Account"],
    }),
    editUser: builder.mutation({
      query: (data) => ({
        url: `/api/accounts/${data.account_id}`,
        body: data.data,
        method: "put",
      }),
      providesTags: ["Account"],
      invalidatesTags: ["Account"],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      providesTags: ["Token"],
    }),
    signUp: builder.mutation({
      query: (data) => ({
        url: "/api/accounts",
        method: "post",
        body: data,
        credentials: "include",
      }),
      providesTags: ["Account"],
      invalidatesTags: (result) => {
        return (result && ["Token"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearForm());
        } catch (err) {}
      },
    }),
    logIn: builder.mutation({
      query: (info) => {
        let formData = null;
        if (info instanceof HTMLElement) {
          formData = new FormData(info);
        } else {
          formData = new FormData();
          formData.append("username", info.username);
          formData.append("password", info.password);
        }
        return {
          url: "/token",
          method: "post",
          body: formData,
          credentials: "include",
        };
      },
      providesTags: ["Account"],
      invalidatesTags: (result) => {
        return (result && ["Token"]) || [];
      },
      async onQueryStarted(arg, { dispatch, queryFulfilled }) {
        try {
          await queryFulfilled;
          dispatch(clearForm());
        } catch (err) {}
      },
    }),
    logOut: builder.mutation({
      query: () => ({
        url: "/token",
        method: "delete",
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Token", "Cart"],
    }),
  }),
});

export const {
    useGetStoresQuery,
    useGetStoreQuery,
    useCreateStoreMutation,
    useGetTokenQuery,
    useLogInMutation,
    useSignUpMutation,
    useLogOutMutation,
    useCreateProductMutation,
    useGetStoreProductsQuery,
    useGetProductQuery,
    useEditStoreMutation,
    useGetCartQuery,
    useLazyGetStoreQuery,
    useEditProductMutation,
    useGetUserQuery,
    useEditUserMutation,
    useAddProductToCartMutation,
    useGetCartProductsQuery,
    useClearCartMutation,
    useRemoveProductFromCartMutation,
    useRemoveProductFromStoreMutation,
    useCreateCartMutation,
    useLazyGetTokenQuery,
    useCheckoutCartMutation,
    useEditCartMutation,
    useGetUserPurchasesQuery
} = apiSlice;
