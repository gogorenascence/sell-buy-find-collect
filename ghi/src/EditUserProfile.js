import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useEditUserMutation } from "./store/api";


function EditUserProfile() {
  const { account_id } = useParams();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [pokemon, setPokemon] = useState("");
  const [zodiac, setZodiac] = useState("");

  const fetchData = async () => {
    const url = `${process.env.REACT_APP_API_HOST}/api/accounts/${account_id}`;
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setUsername(data?.username);
      setEmail(data?.email);
      setPokemon(data?.pokemon);
      setZodiac(data?.zodiac);
    }
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (result.isSuccess) {
      navigate(`/accounts/${account_id}`);
    }
  });

  useEffect(() => {
    fetchData();
  // eslint-disable-next-line
  }, []);

  const [editUser, result] = useEditUserMutation();

  const handleUsernameChange = (event) => {
    const value = event.target.value;
    setUsername(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  const handlePokemonChange = (event) => {
    const value = event.target.value;
    setPokemon(value);
  };

  const handleZodiacChange = (event) => {
    const value = event.target.value;
    setZodiac(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      data: {
        username,
        email,
        pokemon,
        zodiac,
      },
      account_id,
    };
    editUser(data);
  };

  if (result.isSuccess) {
    console.log("Profile update successful!");
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Edit Your Profile</h1>
          <form onSubmit={handleSubmit} id="update-profile-form">
            <div className="form-floating mb-3">
              <input
                value={username}
                onChange={handleUsernameChange}
                placeholder="Username"
                required
                type="text"
                id="username"
                className="form-control"
              />
              <label htmlFor="username">Username</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={email}
                onChange={handleEmailChange}
                placeholder="Email"
                required
                type="text"
                id="email"
                className="form-control"
              />
              <label htmlFor="email">Email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={pokemon}
                onChange={handlePokemonChange}
                placeholder="Pokemon"
                required
                type="text"
                id="pokemon"
                className="form-control"
              />
              <label htmlFor="pokemom">Pokemon</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={zodiac}
                onChange={handleZodiacChange}
                placeholder="Zodiac"
                required
                type="text"
                id="zodiac"
                className="form-control"
              />
              <label htmlFor="Zodiac">Zodiac</label>
            </div>
            <button className="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditUserProfile;
