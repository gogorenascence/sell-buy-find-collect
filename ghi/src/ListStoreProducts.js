import React, { useEffect } from "react";
import { useParams, Link, useNavigate } from "react-router-dom";
import {
  useGetStoreProductsQuery,
  useGetStoreQuery,
  useGetTokenQuery,
  useRemoveProductFromStoreMutation,
} from "./store/api";
import { Decimal } from "decimal.js";

function DollarDisplay(props) {
  const { price } = props;
  if (typeof price !== "number") {
    return "";
  }
  const dollars = new Decimal(price).toFixed(2);
  return dollars;
}

function ListStoreProducts() {
  const { store_id } = useParams();
  const { data } = useGetStoreProductsQuery(store_id);
  const { data: store_data } = useGetStoreQuery(store_id);
  const { data: tokenData } = useGetTokenQuery();
  const [removeProduct, removeResult] = useRemoveProductFromStoreMutation();

  const navigate = useNavigate();
  useEffect(() => {
    if (removeResult.isSuccess) {
      navigate(`/stores/${tokenData?.account?.store_id}`);
    }
  });

  return (
    <>
      <header className="bg-dark py-5">
        <div className="container px-4 px-lg-5 my-5">
          <div className="text-center text-white">
            <h1 className="display-4 fw-bolder">{store_data?.store_name}</h1>
            <p className="lead fw-normal text-white-50 mb-0">
              {store_data?.description}
            </p>
          </div>
        </div>
      </header>
      <section className="py-5">
        <div className="container px-4 px-lg-5 mt-5">
          <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            {data?.products?.map((product) => {
              return (
                <div className="col mb-5" key={product.id}>
                  <div className="card h-100">
                    <img
                      src={product.product_img_url}
                      className="card-img-top"
                      alt="product"
                      width="450"
                      height="300"
                    />
                    <div className="card-body p-4">
                      <div className="text-center">
                        <h5 className="fw-bolder">{product.product_name}</h5>
                        $<DollarDisplay price={product.product_price} />
                      </div>
                    </div>
                    <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                      <div className="text-center mb-3">
                        <Link
                          className="btn btn-outline-dark mt-auto"
                          to={`/products/${product.id}`}
                        >
                          View Product
                        </Link>
                      </div>
                      <div className="d-flex justify-content-center">
                        <div className="text-center">
                          {tokenData?.account?.store_id === store_id ? (
                            <Link
                              to={`../../products/${product.id}/edit/`}
                              className=""
                            >
                              <button
                                className="btn btn-outline-success flex-shrink-0 btn-sm"
                                type="button"
                              >
                                Update
                              </button>
                            </Link>
                          ) : (
                            ""
                          )}
                        </div>
                        <div className="text-center">
                          {tokenData?.account?.store_id === store_id ? (
                            <button
                              onClick={() =>
                                removeProduct({
                                  store_id: store_id,
                                  product_id: product.id,
                                })
                              }
                              className="btn btn-outline-danger flex-shrink-0 btn-sm"
                              type="button"
                            >
                              Delete
                            </button>
                          ) : (
                            ""
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
}

export default ListStoreProducts;
