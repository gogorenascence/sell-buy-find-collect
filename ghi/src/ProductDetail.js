import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  useGetProductQuery,
  useCreateCartMutation,
  useLazyGetStoreQuery,
  useAddProductToCartMutation,
  useGetTokenQuery,
} from "./store/api";
import { CiSquarePlus, CiSquareMinus } from 'react-icons/ci';
import { AiOutlineShoppingCart } from "react-icons/ai";
import {Decimal} from 'decimal.js'

function DollarDisplay(props) {
  const { price } = props;
  if (typeof price !== "number") {
    return "";
  }
  const dollars = new Decimal(price).toFixed(2);
  return dollars;
}

function AddToCart() {
  const { product_id } = useParams();
  const { data: tokenData } = useGetTokenQuery();
  const [addProductToCart, addProductToCartResult] = useAddProductToCartMutation();
  const [createCart, cart_result] = useCreateCartMutation();
  const [counter, setCounter] = useState(1);
  const incrementCounter = () => setCounter(counter + 1);
  let decrementCounter = () => setCounter(counter - 1);
  if (counter === 1) {
    decrementCounter = () => setCounter(1);
  }

  const data = {
    cart_id: tokenData?.account?.cart_id,
    product_id,
    quantity: counter,
  };
  const cart_id = data?.cart_id;
  const cart_data = {
    cart_products: [],
    total_cost: 0,
    account_id: tokenData?.account?.id,
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (cart_id === null) {
      createCart(cart_data);
      return;
    }
    addProductToCart(data);
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (addProductToCartResult.isSuccess) {
      navigate(`../../cart/${tokenData?.account?.cart_id}`);
    }
  });

  useEffect(() => {
    if (cart_result?.data?.id) {
      addProductToCart({
        product_id,
        cart_id: cart_result.data.id,
        quantity: counter,
      });
    }
    // eslint-disable-next-line
  }, [cart_result]);

  return (
    <>
      <div>
        <CiSquarePlus
          size={23}
          className="mb-2 mt-2 mr-2"
          onClick={incrementCounter}
        />
        <display message={counter}> {counter} </display>
        <CiSquareMinus
          size={23}
          className="mb-2 mt-2 ml-2"
          onClick={decrementCounter}
        />
      </div>
      <div className="d-flex">
        <button
          className="btn btn-outline-dark flex-shrink-0"
          type="button"
          onClick={handleSubmit}
        >
          <AiOutlineShoppingCart size={20} />
          Add to Cart
        </button>
      </div>
    </>
  );
}

function ProductDetail() {
  const { product_id } = useParams();
  const { data } = useGetProductQuery(product_id);
  const [trigger] = useLazyGetStoreQuery();

  useEffect(() => {
    if (data?.store_id) {
      trigger(data?.store_id);
    }
  // eslint-disable-next-line
  },[]);

  return (
    <>
      <section className="py-5">
        <div className="container px-4 px-lg-5 my-5">
          <div className="row gx-4 gx-lg-5 align-items-center">
            <div className="col-md-6">
              <img
                className="card-img-top mb-5 mb-md-0"
                src={data?.product_img_url}
                alt="product"
              />
            </div>
            <div className="col-md-6">
              <h1 className="display-5 fw-bolder">{data?.product_name}</h1>
              <div className="fs-5 mb-5">
                <span>
                  $<DollarDisplay price={data && data.product_price} />
                </span>
              </div>
              <p className="lead">{data?.product_description}</p>
              <AddToCart />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default ProductDetail;
