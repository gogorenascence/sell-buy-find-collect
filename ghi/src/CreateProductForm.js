import React, { useState, useEffect } from "react";
import { useCreateProductMutation, useGetTokenQuery } from "./store/api";
import { useNavigate } from "react-router-dom"


function CreateProductForm() {
  const [product_name, setProductName] = useState("");
  const [product_description, setProductDescription] = useState("");
  const [product_img_url, setProductImgUrl] = useState("");
  const [product_price, setProductPrice] = useState("");
  const [createProduct, result] = useCreateProductMutation();

  const { data: tokenData } = useGetTokenQuery();

  const handleProductNameChange = (event) => {
    const value = event.target.value;
    setProductName(value);
  };

  const handleProductDescriptionChange = (event) => {
    const value = event.target.value;
    setProductDescription(value);
  };

  const handleProductImgUrlChange = (event) => {
    const value = event.target.value;
    setProductImgUrl(value);
  };

  const handleProductPriceChange = (event) => {
    const value = event.target.value;
    setProductPrice(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    createProduct({
      product_name,
      product_description,
      product_img_url,
      product_price,
      store_id: tokenData.account.store_id,
    });
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (result.isSuccess) {
      navigate(`/stores/${tokenData.account.store_id}`);
    }
  });

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Product</h1>
          <form onSubmit={handleSubmit} id="create-product-form">
            <div className="form-floating mb-3">
              <input
                value={product_name}
                onChange={handleProductNameChange}
                placeholder="Product Name"
                required
                type="text"
                id="product_name"
                className="form-control"
              />
              <label htmlFor="product_name">Product Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={product_description}
                onChange={handleProductDescriptionChange}
                placeholder="Product Description"
                required
                type="text"
                id="product_description"
                className="form-control"
              />
              <label htmlFor="product_description">Description</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={product_img_url}
                onChange={handleProductImgUrlChange}
                placeholder="Product Image Url"
                type="url"
                id="product_img_url"
                className="form-control"
              />
              <label htmlFor="product_img_url">Image Url</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={product_price}
                onChange={handleProductPriceChange}
                placeholder="Product Price"
                type="number"
                id="product_price"
                className="form-control"
              />
              <label htmlFor="product_price">Price</label>
            </div>
            <button className="btn btn-primary">Add</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateProductForm;
