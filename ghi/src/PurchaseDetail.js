import { useParams } from "react-router-dom";
import { useGetCartProductsQuery } from "./store/api";

function DollarDisplay(props) {
  const { price } = props;
  const dollars = price.toFixed(2);
  return dollars;
}

function PurchaseDetail() {
  const { cart_id } = useParams();
  const { data: cart_product_data } = useGetCartProductsQuery(cart_id);

  let total = 0;
  if (cart_product_data?.products) {
    for (let product of cart_product_data?.products) {
      total += product.product_price * product.quantity;
    }
  };
  return (
    <>
      <section className="h-100 h-custom">
        <div className="container h-100 py-5">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col">
              <div className="text-center mb-5">
                <h1 style={{ "font-weight": "380" }}>Order ID: {cart_id}</h1>
              </div>
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        PRODUCT
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        STORE
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        QUANTITY
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        PRICE
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {cart_product_data?.products.map((product) => {
                      return (
                        <tr key={product.id}>
                          <th scope="row">
                            <div className="d-flex align-items-center">
                              <img
                                src={product.product_img_url}
                                className="img-fluid rounded-3 m-3"
                                style={{ width: "100px", height: "100px" }}
                                alt="Product"
                              />
                              <div className="flex-column ms-4">
                                <p
                                  className="mb-0"
                                  style={{ "font-weight": "400" }}
                                >
                                  {product.product_name}
                                </p>
                              </div>
                            </div>
                          </th>
                          <td className="align-middle">
                            <p
                              className="mb-0"
                              style={{ "font-weight": "400" }}
                            >
                              {product.store_name}
                            </p>
                          </td>
                          <td className="align-middle">
                            <div
                              className="d-flex flex-row"
                              style={{ "font-weight": "400" }}
                            >
                              {product.quantity}
                            </div>
                          </td>
                          <td className="align-middle">
                            <p
                              className="mb-0"
                              style={{ "font-weight": "400" }}
                            >
                              $<DollarDisplay price={product.product_price} />
                            </p>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div
                className="text-end mt-4 me-4"
                style={{ "font-weight": "500" }}
              >
                <h5>
                  Total: $<DollarDisplay price={total} />
                </h5>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default PurchaseDetail;
