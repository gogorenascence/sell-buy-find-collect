import { useGetStoresQuery } from "./store/api";
import { Link } from "react-router-dom";


function ListStores() {
  const { data } = useGetStoresQuery();
  return (
    <>
      <header className="bg-white py-5">
        <div className="container px-4 px-lg-5 my-5">
          <div className="text-center text-dark">
            <h1 className="home-display display-3 fw-bolder">SBFC</h1>
            <p className="lead fw-normal text-black-50 mb-0">
              Sell Buy Find Collect
            </p>
          </div>
        </div>
      </header>
      <section className="py-5">
        <div className="container px-4 px-lg-5 mt-5">
          <div className="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
            {data?.stores.map((store) => {
              return (
                <div className="col mb-5" key={store.id}>
                  <div className="card h-100">
                    <img
                      src={store.img_url}
                      className="card-img-top"
                      alt="store"
                      width="450"
                      height="300"
                    />
                    <div className="card-body p-4">
                      <div className="text-center">
                        <h5 className="fw-bolder">{store.store_name}</h5>
                      </div>
                    </div>
                    <div className="card-footer p-4 pt-0 border-top-0 bg-transparent">
                      <div className="text-center">
                        <Link
                          className="btn btn-outline-dark mt-auto"
                          to={`/stores/${store.id}`}
                        >
                          View Products
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
}

export default ListStores;
