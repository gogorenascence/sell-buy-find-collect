import { useParams, useNavigate } from "react-router-dom";
import { useEffect } from "react";
import {
  useGetTokenQuery,
  useCheckoutCartMutation,
  useGetCartProductsQuery,
  useRemoveProductFromCartMutation,
  useEditCartMutation,
  useGetCartQuery,
} from "./store/api";

function DollarDisplay(props) {
  const { price } = props;
  const dollars = price.toFixed(2);
  return dollars;
}

function ShoppingCart() {
  const { cart_id } = useParams();
  const { data: cart_product_data } = useGetCartProductsQuery(cart_id);
  const { data: cart_data } = useGetCartQuery(cart_id);
  const [removeProduct] = useRemoveProductFromCartMutation();
  const [checkOut, result] = useCheckoutCartMutation();
  const [editCart] = useEditCartMutation();
  const { data: tokenData } = useGetTokenQuery();
  const account_id = tokenData?.account?.id;
  const navigate = useNavigate();

  useEffect(() => {
    if (result.isSuccess) {
      navigate(`/accounts/${account_id}/purchases/${cart_id}`);
    }
  });

  let centsForConversion = 0;
  if (cart_product_data?.products) {
    for (let product of cart_product_data?.products) {
      centsForConversion += product.product_price * 100 * product.quantity;
    }
  }
  let dollars = Math.floor(centsForConversion / 100);
  let cents = centsForConversion % 100;
  let centsToDecimal = cents / 100;
  let total = dollars + centsToDecimal;
  let data = {
    cart_id: cart_id,
    data: {
      cart_products: cart_data?.cart_products,
      total_cost: total,
      account_id: account_id,
    },
  };

  return (
    <>
      <section className="h-100 h-custom">
        <div className="container h-100 py-5">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col">
              <div className="text-center mb-5">
                <h1 style={{ "font-weight": "380" }}>Your Cart</h1>
              </div>
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        PRODUCT
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        STORE
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        QUANTITY
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        PRICE
                      </th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {cart_product_data?.products.map((product) => {
                      return (
                        <tr key={product.id}>
                          <th scope="row">
                            <div className="d-flex align-items-center">
                              <img
                                src={product.product_img_url}
                                className="img-fluid rounded-3 m-3"
                                style={{ width: "100px", height: "100px" }}
                                alt="Product"
                              />
                              <div className="flex-column ms-4">
                                <p
                                  className="mb-0"
                                  style={{ "font-weight": "400" }}
                                >
                                  {product.product_name}
                                </p>
                              </div>
                            </div>
                          </th>
                          <td className="align-middle">
                            <p
                              className="mb-0"
                              style={{ "font-weight": "400" }}
                            >
                              {product.store_name}
                            </p>
                          </td>
                          <td className="align-middle">
                            <div
                              className="d-flex flex-row"
                              style={{ "font-weight": "400" }}
                            >
                              {product.quantity}
                            </div>
                          </td>
                          <td className="align-middle">
                            <p
                              className="mb-0"
                              style={{ "font-weight": "400" }}
                            >
                              $<DollarDisplay price={product.product_price} />
                            </p>
                          </td>
                          <td className="align-middle">
                            <button
                              onClick={() =>
                                removeProduct({
                                  cart_id,
                                  product_id: product.id,
                                })
                              }
                              className="btn btn-outline-danger flex-shrink-0 btn-sm"
                            >
                              Remove
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
              <div
                className="text-end mt-4 me-4"
                style={{ "font-weight": "500" }}
              >
                <h5>
                  Total: $<DollarDisplay price={total} />
                </h5>
              </div>
              <div className="text-end mt-4 me-4">
                <button
                  onClick={() => {
                    editCart(data);
                    checkOut(account_id);
                  }}
                  className="btn btn-dark mt-auto"
                  type="button"
                >
                  Check Out
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default ShoppingCart;
