import {
  useGetTokenQuery,
  useGetUserPurchasesQuery,
} from "./store/api";
import { Link } from "react-router-dom";

function DollarDisplay(props) {
  const { price } = props;
  const dollars = price.toFixed(2);
  return dollars;
}

function PurchaseHistory() {
  const { data: tokenData } = useGetTokenQuery();
  const account_id = tokenData?.account?.id;
  const { data: userPurchasesData } = useGetUserPurchasesQuery(account_id);
  return (
    <>
      <section className="h-100 h-custom">
        <div className="container h-100 py-5">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col">
              <div className="text-center mb-5">
                <h1 style={{ "font-weight": "380" }}>Order History</h1>
              </div>
              <div className="table-responsive">
                <table className="table">
                  <thead>
                    <tr>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        ORDER ID
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        QUANTITY
                      </th>
                      <th scope="col" style={{ "font-weight": "500" }}>
                        TOTAL
                      </th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {userPurchasesData?.carts.map((purchase) => {
                      return (
                        <tr key={purchase.id}>
                          <td className="align-middle">{purchase.id}</td>
                          <td className="align-middle">
                            {purchase.cart_products.length}
                          </td>
                          <td className="align-middle">
                            <p
                              className="mb-0"
                              style={{ "font-weight": "400" }}
                            >
                              $<DollarDisplay price={purchase.total_cost} />
                            </p>
                          </td>
                          <td className="align-middle">
                            <Link
                              className="btn btn-outline-dark mt-auto btn-sm"
                              to={`${purchase.id}`}
                            >
                              View Order
                            </Link>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}

export default PurchaseHistory;
