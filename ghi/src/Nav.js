import React, { useEffect } from "react";
import { Link, useNavigate } from 'react-router-dom';
import { useGetCartQuery, useGetTokenQuery, useLogOutMutation } from './store/api';
import { AiOutlineShoppingCart } from 'react-icons/ai';


function LogOut() {
    const [logOut, result] = useLogOutMutation();
    const navigate = useNavigate();
    useEffect(() => {
    if (result.isSuccess) {
        navigate("/");
    }
    });
    return (
      <div className="buttons">
        <Link onClick={logOut} className="dropdown-item">
          Log out
        </Link>
      </div>
    );
};


function Nav() {
    const { data: token, isLoading: tokenLoading } = useGetTokenQuery();
    const cart_id = token?.account?.cart_id;
    const { data: cartData } = useGetCartQuery(cart_id);

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <div className="navbar-brand ms-1 mb-1">
            <Link className="navbar-brand pe-10" to="/">
              <img
                src="https://i.imgur.com/EoKreQT.png"
                width="30"
                height="30"
                className="d-inline-block align-top"
                alt="SBFC logo"
              />
            </Link>
          </div>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
              <li className="nav-item">
                <Link className="nav-link active" aria-current="page" to="/">
                  Home
                </Link>
              </li>
              <li className="nav-item">
                {token?.account?.store_id === null && token ? (
                  <Link className="nav-link active" to="/stores/new">
                    Create Store
                  </Link>
                ) : (
                  ""
                )}
              </li>
            </ul>
            <ul className="navbar-nav mb-2 mb-lg-0 ms-lg-4">
              <li className="nav-item dropdown">
                {/* eslint-disable-next-line */}
                <a
                  className="nav-link dropdown-toggle"
                  id="navbarDropdown"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <img
                    src={token?.account?.pokemon}
                    alt="pokemon"
                    width="40"
                    height="40"
                  />
                </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li>
                    {tokenLoading ? (
                      ""
                    ) : token ? (
                      <Link
                        className="dropdown-item"
                        to={`accounts/${token?.account?.id}`}
                      >
                        Profile
                      </Link>
                    ) : (
                      <Link className="dropdown-item" to="/signup">
                        Sign Up
                      </Link>
                    )}
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    {tokenLoading ? (
                      ""
                    ) : token ? (
                      <LogOut />
                    ) : (
                      <Link
                        className="dropdown-item"
                        to="/login"
                        style={{ textDecoration: "none" }}
                      >
                        Log In
                      </Link>
                    )}
                  </li>
                </ul>
              </li>
            </ul>
            {/* <div>
              {token?.account?.cart_id != null ? (
                <Link className="" to={`cart/${token?.account?.cart_id}`}>
                  <AiOutlineShoppingCart size={25} />(
                  {cartData?.cart_products?.length})
                </Link>
              ) : (
                ""
              )}
            </div> */}
            <form className="d-flex">
              {token?.account?.cart_id != null ? (
                <Link className="" to={`cart/${token?.account?.cart_id}`}>
                  <button className="btn btn-outline-dark" type="submit">
                    <AiOutlineShoppingCart size={20} />
                    Cart
                    <span className="badge bg-dark text-white ms-1 rounded-pill">
                      {cartData?.cart_products?.length}
                    </span>
                  </button>
                </Link>
              ) : (
                ""
              )}
            </form>
          </div>
        </div>
      </nav>
    );
}


export default Nav;
