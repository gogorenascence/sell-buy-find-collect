import os
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from authenticator import authenticator

from routers import accounts, stores, products, cart

app = FastAPI()
app.include_router(authenticator.router)
app.include_router(accounts.router)
app.include_router(stores.router)
app.include_router(products.router)
app.include_router(cart.router)


origins = [
    os.environ.get("REACT_APP_API_HOST", None),
    os.environ.get("CORS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000"),
        os.environ.get("CORS_HOST", "http://localhost:8000"),
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
async def health_check():
    return {"Donut": "Donut"}
