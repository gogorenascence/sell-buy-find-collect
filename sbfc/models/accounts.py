from pydantic import BaseModel
from typing import Optional
from bson.objectid import ObjectId


class PydanticObjectId(ObjectId):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value: ObjectId | str) -> ObjectId:
        if value:
            try:
                ObjectId(value)
            except ValueError:
                raise ValueError(f"Not a valid object id: {value}")
        return value


class AccountIn(BaseModel):
    username: str
    password: str
    email: str
    pokemon: str = ""
    zodiac: str = ""
    store_id: Optional[str]
    cart_id: Optional[str]
    purchases: list = []


class Account(AccountIn):
    id: PydanticObjectId


class AccountUpdate(BaseModel):
    username: str
    email: str
    pokemon: str
    zodiac: str


class AccountOut(AccountIn):
    id: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountOutUpdate(AccountUpdate):
    id: str


class SessionOut(BaseModel):
    jti: str
    account_id: str


class AccountAll(BaseModel):
    accounts: list
