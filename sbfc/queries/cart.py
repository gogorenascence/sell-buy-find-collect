from .client import Queries
from models.cart import CartIn, CartOut, CartAll
from bson.objectid import ObjectId
from pymongo import ReturnDocument


class DuplicateAccountError(ValueError):
    pass


class CartQueries(Queries):
    DB_NAME = "mongodata"
    COLLECTION = "carts"

    def get_cart(self, cart_id: str) -> CartOut:
        props = self.collection.find_one({"_id": ObjectId(cart_id)})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return CartOut(**props)

    def create_cart(self, cart: CartIn) -> CartOut:
        props = cart.dict()
        self.collection.insert_one(props)
        props["id"] = str(props["_id"])
        return CartOut(**props)

    def get_all_carts(self) -> CartAll:
        db = self.collection.find()
        stores = []
        for document in db:
            document["id"] = str(document["_id"])
            stores.append(CartOut(**document))
        return stores

    def delete_cart(self, cart_id: str) -> bool:
        return self.collection.delete_one({"_id": ObjectId(cart_id)})

    def update_cart(self, cart_id: str, cart: CartIn) -> CartOut:
        props = cart.dict()
        self.collection.find_one_and_update(
            {"_id": ObjectId(cart_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return CartOut(**props, id=cart_id)

    def add_product_to_cart(
        self, cart_id: str, product_id: str, quantity: int
    ) -> CartOut:
        props = self.collection.find_one({"_id": ObjectId(cart_id)})
        cart_products = props["cart_products"]
        for product in cart_products:
            if product.get("product_id") == product_id:
                new_quantity = product.get("quantity") + quantity
                updated_product = {
                    "product_id": product_id,
                    "quantity": new_quantity,
                }
                cart_products.append(updated_product)
                cart_products.remove(product)
                self.collection.find_one_and_update(
                    {"_id": ObjectId(cart_id)},
                    {"$set": props},
                    return_document=ReturnDocument.AFTER,
                )
                return CartOut(**props, id=cart_id)
        product_to_add = {
            "product_id": product_id,
            "quantity": quantity,
        }
        cart_products.append(product_to_add)
        self.collection.find_one_and_update(
            {"_id": ObjectId(cart_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return CartOut(**props, id=cart_id)

    def remove_product_from_cart(
        self, cart_id: str, product_id: str
    ) -> CartOut:
        props = self.collection.find_one({"_id": ObjectId(cart_id)})
        cart_products = props["cart_products"]
        for product in cart_products:
            if product.get("product_id") == product_id:
                cart_products.remove(product)
        self.collection.find_one_and_update(
            {"_id": ObjectId(cart_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return CartOut(**props, id=cart_id)

    def clear_cart(self, cart_id: str) -> CartOut:
        props = self.collection.find_one({"_id": ObjectId(cart_id)})
        props["cart_products"] = []
        self.collection.find_one_and_update(
            {"_id": ObjectId(cart_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return CartOut(**props, id=cart_id)
