from .client import Queries
from models.accounts import (
    Account,
    AccountIn,
    AccountOut,
    AccountAll,
    AccountOutUpdate,
    AccountUpdate,
)
from models.cart import CartAll
from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId
from pymongo import ReturnDocument, MongoClient
import os
import requests
import json


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(Queries):
    DB_NAME = "mongodata"
    COLLECTION = "accounts"

    def create(self, account: AccountIn, hashed_password: str) -> AccountOut:
        props = account.dict()
        props["password"] = hashed_password
        try:
            self.collection.insert_one(props)
        except DuplicateKeyError:
            raise DuplicateAccountError()
        props["id"] = str(props["_id"])
        return Account(**props)

    def get(self, username: str) -> AccountOut:
        props = self.collection.find_one({"username": username})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return Account(**props)

    def get_account(self, account_id: str) -> AccountOut:
        props = self.collection.find_one({"_id": ObjectId(account_id)})
        if not props:
            return None
        props["id"] = str(props["_id"])
        return AccountOut(**props)

    def get_all_accounts(self) -> AccountAll:
        db = self.collection.find()
        accounts = []
        for document in db:
            document["id"] = str(document["_id"])
            accounts.append(AccountOut(**document))
        return accounts

    def update_account(
        self, account_id: str, account: AccountUpdate
    ) -> AccountOutUpdate:
        props = account.dict()
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOutUpdate(**props, id=account_id)

    def delete_account(self, account_id: str) -> bool:
        return self.collection.delete_one({"_id": ObjectId(account_id)})

    def assign_store(self, account_id: str, store_id: str) -> AccountOut:
        props = self.collection.find_one({"_id": ObjectId(account_id)})
        props["store_id"] = store_id
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOut(**props, id=account_id)

    def remove_store(self, account_id: str) -> AccountOut:
        props = self.collection.find_one({"_id": ObjectId(account_id)})
        props["store_id"] = ""
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOut(**props, id=account_id)

    def assign_cart(self, account_id: str, cart_id: str) -> AccountOut:
        props = self.collection.find_one({"_id": ObjectId(account_id)})
        props["cart_id"] = cart_id
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOut(**props, id=account_id)

    def remove_cart(self, account_id: str) -> AccountOut:
        props = self.collection.find_one({"_id": ObjectId(account_id)})
        props["cart_id"] = None
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOut(**props, id=account_id)

    def checkout_cart(self, account_id: str) -> AccountOut:
        props = self.collection.find_one({"_id": ObjectId(account_id)})
        purchase_list = props["purchases"]
        purchase_list.append(props["cart_id"])
        props["cart_id"] = None
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": props},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOut(**props, id=account_id)

    def get_purchases(self, account_id: str) -> CartAll:
        account = self.collection.find_one({"_id": ObjectId(account_id)})
        purchases = account["purchases"]
        purchases_list = []
        purchases_dict = {}
        for purchase in purchases:
            DATABASE_URL = os.environ["DATABASE_URL"]
            conn = MongoClient(DATABASE_URL)
            db = conn.mongodata.carts
            props = db.find_one({"_id": ObjectId(purchase)})
            props["id"] = str(props["_id"])
            props.pop("_id")
            if not props:
                return None
            purchases_list.append(props)
        purchases_dict["carts"] = purchases_list
        return purchases_dict

    def assign_pokemon(self, account_id: str) -> AccountOut:
        account = self.collection.find_one({"_id": ObjectId(account_id)})
        pokemon = account["pokemon"]
        pokemon_url = f"https://pokeapi.co/api/v2/pokemon/{pokemon}"
        response = requests.get(pokemon_url)
        content = json.loads(response.content)
        account["pokemon"] = content["sprites"]["front_default"]
        self.collection.find_one_and_update(
            {"_id": ObjectId(account_id)},
            {"$set": account},
            return_document=ReturnDocument.AFTER,
        )
        return AccountOut(**account, id=account_id)
