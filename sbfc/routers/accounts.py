from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from authenticator import authenticator
from jwtdown_fastapi.authentication import Token
from pydantic import BaseModel
from queries.accounts import AccountQueries, DuplicateAccountError
from models.accounts import (
    AccountIn,
    AccountOut,
    AccountAll,
    AccountOutUpdate,
    AccountUpdate,
)
from models.cart import CartAll


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/api/protected", response_model=bool)
async def get_protected(
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return True


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
    repo: AccountQueries = Depends(),
) -> AccountToken | None:
    account_info = repo.get_account(account["id"])
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account_info,
        }


@router.get("/api/accounts/{account_id}", response_model=AccountOut)
def get_account(account_id, repo: AccountQueries = Depends()):
    account = repo.get_account(account_id)
    return account


@router.get("/api/accounts", response_model=AccountAll)
def get_all_accounts(repo: AccountQueries = Depends()):
    return AccountAll(accounts=repo.get_all_accounts())


@router.put("/api/accounts/{account_id}", response_model=AccountOutUpdate)
async def update_account(
    account_id: str,
    account: AccountUpdate,
    response: Response,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account = repo.update_account(account_id, account)
    return account


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = accounts.create(info, hashed_password)
        accounts.assign_pokemon(account.id)
    except DuplicateAccountError:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create an account with those credentials",
        )
    form = AccountForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.delete("/api/accounts/{account_id}", response_model=bool)
async def delete_account(
    account_id: str,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    repo.delete_account(account_id)
    return True


@router.put(
    "/api/accounts/{account_id}/add_store/{store_id}",
    response_model=AccountOut,
)
async def assign_store(
    account_id: str,
    store_id: str,
    response: Response,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account = repo.assign_store(account_id, store_id)
    return account


@router.put(
    "/api/accounts/{account_id}/remove_store", response_model=AccountOut
)
async def remove_store(
    account_id: str,
    response: Response,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account = repo.remove_store(account_id)
    return account


@router.put(
    "/api/accounts/{account_id}/assign_cart/{cart_id}",
    response_model=AccountOut,
)
async def assign_cart(
    account_id: str,
    cart_id: str,
    response: Response,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account = repo.assign_cart(account_id, cart_id)
    return account


@router.put(
    "/api/accounts/{account_id}/remove_cart", response_model=AccountOut
)
async def remove_cart(
    account_id: str,
    response: Response,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account = repo.remove_cart(account_id)
    return account


@router.put(
    "/api/accounts/{account_id}/checkout_cart", response_model=AccountOut
)
async def checkout_cart(
    account_id: str,
    response: Response,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account = repo.checkout_cart(account_id)
    return account


@router.get("/api/accounts/{account_id}/purchases", response_model=CartAll)
def get_purchases(account_id, repo: AccountQueries = Depends()):
    purchases = repo.get_purchases(account_id)
    return purchases
