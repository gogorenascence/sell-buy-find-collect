from fastapi.testclient import TestClient
from main import app
from models.stores import StoreOut
from queries.stores import StoreQueries

client = TestClient(app)


class GetStoreQueries:
    def get_store(self, store_id):
        return {
            "id": 2020,
            "store_name": "Donut Donut",
            "description": "Greatest Donut",
            "img_url": "google.com",
            "account_id": "accountidfordonuttunod",
            "products": [],
        }


def test_get_store():
    # arrange
    app.dependency_overrides[StoreQueries] = GetStoreQueries

    storeout = StoreOut(
        id=2020,
        store_name="Donut Donut",
        description="Greatest Donut",
        img_url="google.com",
        account_id="accountidfordonuttunod",
        products=[],
    )

    # act
    response = client.get("/api/stores/2020")

    # cleanup
    app.dependency_overrides = {}

    # assert
    assert response.status_code == 200
    assert response.json() == storeout.dict()
